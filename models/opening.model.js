const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const openingSchema = new Schema({
    username: { type: String, required: true },
    openedTime: { type: Date, default: Date.now }
});

const Opening = mongoose.model('Opening', openingSchema);

module.exports = Opening;