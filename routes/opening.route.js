const router = require('express').Router();
const Opening = require('../models/opening.model');

router.route('/').get((req, res) => {
    Opening.find()
        .then(openings => res.json(openings))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    const username = req.body.username;
    const newOpening = new Opening({username});
    newOpening.save()
        .then(() => res.json('Opening added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;